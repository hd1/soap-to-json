require 'json'
class JsonController < ApplicationController
  include JSON
  def json
    @wsdl_url = params[:url]
    if @wsdl_url.nil?
      @endpoint = params[:endpoint]
      @namespace = params[:namespace]
      @client = Savon.client(@endpoint, @namespace)
    else
      @client = Savon.client(wsdl: @wsdl_url)
    end
    @operations = @client.operations
    render text:pretty_generate(@operations)
  end

  def self.method_messing(meth, args, &block)
    if @operations.member? meth
      return @client.call(meth, message: args)
    end
    super
  end
end
